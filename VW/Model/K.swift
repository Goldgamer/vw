//
//  K.swift
//  K
//
//  Created by Max Vincent Goldgamer on 06.09.21.
//

enum K {
    static let appName = "VersionsVerwaltung"
    static let versionCellIdentifier = "VersionCell"
    static let versionCellNibName = "VersionCell"
    static let programCellIdentifier = "ProgramCell"
    static let programCellNibName = "ProgramCell"
    static let versionSegue = "versionSegue"
    static let programSegue = "programSegue"
    
    static var version: [Version] = []
    static var program: [Program] = []
    
    static var myVersion: [Version] = []
    static var myVersionName: String?
    static var myVersionID: Int!
    
    static var versionPointer: Int = 0
    
    enum FStore {
        static let programCollectionName = "Programme"
        static let versionCollectionName = "Versionen"
        static let nameField = "name"
        static let versionField = "version"
        static let categoryField = "category"
        static let dateField = "date"
        static let valuationField = "valuation"
        static let idField = "id"
    }
    
    static var shouldReload: Bool = false
    
    static func setMyVersion() {
        K.myVersion = K.version.filter { $0.id == K.version[K.versionPointer].id }
        K.myVersionID = K.program[K.versionPointer].id
    }
    
    static func getCategory(num: Int) -> String {
        switch num {
        case 1:
            return "Dienstleistungen"
        case 2:
            return "Spiele"
        default:
            return ""
        }
    }
}
