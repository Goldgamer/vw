//
//  VersionModel.swift
//  VersionModel
//
//  Created by Max Vincent Goldgamer on 03.09.21.
//

import Foundation

struct Program {
    var softwareName: String
    var category: Int
    var id: Int
}

struct Version {
    var id: Int
    var versionNumber: String
    var date: String
    var valuation: Int
}
