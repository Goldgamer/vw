//
//  VersionCell.swift
//  VersionCell
//
//  Created by Max Vincent Goldgamer on 03.09.21.
//

import UIKit

class VersionCell: UITableViewCell {
    @IBOutlet var versionsNummerLabel: UILabel!
    @IBOutlet var erscheinungsDatumLabel: UILabel!
    @IBOutlet var bewertungsLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
