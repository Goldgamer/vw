//
//  ProgramCell.swift
//  ProgramCell
//
//  Created by Max Vincent Goldgamer on 07.09.21.
//

import UIKit

class ProgramCell: UITableViewCell {
    @IBOutlet var pName: UILabel!
    @IBOutlet var pCategory: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
