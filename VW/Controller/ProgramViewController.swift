//
//  VersionsViewController.swift
//  VersionsViewController
//
//  Created by Max Vincent Goldgamer on 03.09.21.
//

import Firebase
import Foundation
import UIKit

class ProgramViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var hinzufügenButton: UIButton!
    
    let db = Firestore.firestore()
    
    /* var version: [Version] = [
         Version(softwareName: "TestVersion", versionNumber: "1.0b1", category: 1, date: "01.09.2021", valuation: 4.5),
         Version(softwareName: "Zweite App", versionNumber: "1.1b1", category: 2, date: "2.09.2021", valuation: 1.5)
     ]*/
    
    func loadProgramDB() {
        db.collection(K.FStore.programCollectionName)
            .addSnapshotListener { querySnapshot, error in
                K.program = []
                
                if let e = error {
                    print("Error: ", e.localizedDescription)
                } else {
                    if let snapshotDocuments = querySnapshot?.documents {
                        for doc in snapshotDocuments {
                            let data = doc.data()
                            
                            // MARK: Programs

                            if let versionName = data[K.FStore.nameField] as? String, let category = data[K.FStore.categoryField] as? Int, let id = data[K.FStore.idField] as? Int {
                                let newProgramm = Program(softwareName: versionName, category: category, id: id)
                                K.program.append(newProgramm)
                            }
                                
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                                let indexPath = IndexPath(row: K.program.count - 1, section: 0)
                                self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
                            }
                        }
                    }
                }
            }
    }

    func loadVersionDB() {
        db.collection(K.FStore.versionCollectionName)
            .addSnapshotListener { querySnapshot, error in
                
                K.version = []
                
                if let e = error {
                    print("Error: ", e.localizedDescription)
                } else {
                    if let snapshotDocuments = querySnapshot?.documents {
                        for doc in snapshotDocuments {
                            let data = doc.data()
                            
                            // MARK: Versions

                            if let versionNumber = data[K.FStore.versionField] as? String, let date = data[K.FStore.dateField] as? String, let valuation = data[K.FStore.valuationField] as? Int, let id = data[K.FStore.idField] as? Int {
                                let newVersion = Version(id: id, versionNumber: versionNumber, date: date, valuation: valuation)
                                K.version.append(newVersion)
                            }
                        }
                    }
                }
            }
    }
    
    override func viewDidLoad() {
        hinzufügenButton.setTitle("+", for: .normal)
        hinzufügenButton.isHidden = true
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        title = "Programmverwaltung"
        tableView.register(UINib(nibName: K.programCellNibName, bundle: nil), forCellReuseIdentifier: K.programCellIdentifier)
        loadProgramDB()
        loadVersionDB()
    }
}

extension ProgramViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return K.program.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProgramCell", for: indexPath)
            as! ProgramCell
        cell.pName.text = K.program[indexPath.row].softwareName
        cell.pCategory.text = K.getCategory(num: K.program[indexPath.row].category)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        K.myVersionName = K.program[indexPath.row].softwareName
        K.versionPointer = indexPath.row
        K.setMyVersion()
        performSegue(withIdentifier: K.versionSegue, sender: self)
    }
}
