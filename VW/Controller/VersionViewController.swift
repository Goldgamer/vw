//
//  VersionViewController.swift
//  VersionViewController
//
//  Created by Max Vincent Goldgamer on 07.09.21.
//

import Foundation
import UIKit

class VersionViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var hinzufügenButton: UIButton!
    
    @IBAction func hinzufügenButtonAction(_ sender: UIButton) {
        performSegue(withIdentifier: "addVersionSegue", sender: self)
    }
    
    override func viewDidLoad() {
        hinzufügenButton.setTitle("+", for: .normal)
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        title = K.myVersionName ?? "NoName"
        tableView.register(UINib(nibName: K.versionCellNibName, bundle: nil), forCellReuseIdentifier: K.versionCellIdentifier)
    }
   
    override func viewWillAppear(_ animated: Bool) {
        reloadView()
    }
    
    func reloadView() {
        if K.shouldReload {
            K.setMyVersion()
            DispatchQueue.main.async {
                self.tableView.reloadData()
                K.shouldReload = false
            }
        }
    }
}

extension VersionViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return K.myVersion.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VersionCell", for: indexPath)
            as! VersionCell
        
        cell.versionsNummerLabel.text = K.myVersion[indexPath.row].versionNumber
        cell.erscheinungsDatumLabel.text = K.myVersion[indexPath.row].date
        cell.bewertungsLabel.text = String(K.myVersion[indexPath.row].valuation)
       
        return cell
    }
}
