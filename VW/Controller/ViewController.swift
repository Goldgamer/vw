//
//  ViewController.swift
//  VW
//
//  Created by Max Vincent Goldgamer on 03.09.21.
//

import UIKit

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func ButtonAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: K.programSegue, sender: self)
    }
}
