//
//  AddVersionViewController.swift
//  AddVersionViewController
//
//  Created by Max Vincent Goldgamer on 07.09.21.
//

import Firebase
import UIKit

class AddVersionViewController: UIViewController {
    let db = Firestore.firestore()
    
    @IBOutlet var versionLabel: UITextField!
    
    @IBOutlet var dateLabel: UITextField!
    
    @IBOutlet var valuationLabel: UITextField!
    
    @IBAction func addButton(_ sender: UIButton) {
        if versionLabel.text != "", dateLabel.text != "", valuationLabel.text != "" {
            db.collection(K.FStore.versionCollectionName).addDocument(data: [
                K.FStore.versionField: versionLabel.text!,
                K.FStore.dateField: dateLabel.text!,
                K.FStore.valuationField: Int(valuationLabel.text!)!,
                K.FStore.idField: K.myVersionID!
            
            ]) { error in
                if let e = error {
                    print("ERROR:", e.localizedDescription)
                } else {
                    DispatchQueue.main.async {
                        K.shouldReload = true
                        _ = self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.navigationItem.title = K.myVersionName ?? "NoName"
    }
}
